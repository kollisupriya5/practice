
# coding: utf-8

# In[3]:

a_tuple = ('ready', 'fire', 'aim')
list(a_tuple)


# In[8]:

birthday = '27/05/1996'
birthday.split('/')


# In[12]:

splitme = 'a/b//c/d//e'
splitme.split("/")


# In[14]:

splitme = 'a/b//c/d///e'
splitme.split('//')


# In[20]:

marxes = ['Groucho', 'Chico', 'Harpo']
marxes[1]


# In[25]:

small_birds = ['hummingbird', 'finch']
extinct_birds = ['dodo', 'passenger pigeon', 'Norwegian Blue']
carol_birds = [3, 'French hens', 2, 'turtledoves']
all_birds = [small_birds, extinct_birds, 'macaw', carol_birds]


# In[26]:

all_birds


# In[31]:

marxes = ['Groucho', 'Chico', 'Harpo']
marxes[2] = 'Wanda'
marxes


# In[34]:

marxes[1]='finch'
marxes


# In[40]:

marxes = ['Groucho', 'Chico', 'Harpo']
marxes[0:2]


# In[78]:

marxes = ['Groucho', 'Chico', 'Harpo']
marxes.append('finch')
marxes


# In[79]:

others = ['Gummo', 'Karl']
marxes.extend(others)
marxes


# In[76]:

marxes+=others
marxes


# In[111]:

marxes = ['Groucho', 'Chico', 'Harpo']
marxes.insert(3, 'others')
marxes


# In[112]:

marxes.insert(2,'karl')
marxes


# In[113]:

marxes.insert(-2,'finch')
marxes


# In[114]:

del marxes[-2]
marxes


# In[115]:

marxes.remove('karl')
marxes


# In[117]:

marxes.remove('Chico')
marxes


# In[118]:

marxes = ['Groucho', 'Chico', 'Harpo', 'Zeppo']
marxes.pop()


# In[123]:

marxes = ['Groucho', 'Chico', 'Harpo', 'Zeppo']
marxes.index('Groucho')


# In[128]:

words = ['a', 'deer', 'a' 'female', 'deer']
'deer' in words


# In[133]:

snl_skit = ['cheeseburger', 'burger', 'cheese']
snl_skit.count('cheeseburger')


# In[137]:

marxes = ['Groucho', 'Chico', 'Harpo']
', '.join(marxes)


# In[139]:

friends = ['Harry', 'Hermione', 'Ron']
separator = ' / '
joined = separator.join(friends)
joined


# In[142]:

separated = joined.split(separator)
separated


# In[143]:

separated == friends


# In[146]:

marxes = ['Groucho', 'Chico', 'Harpo','Apple']
sorted_marxes = sorted(marxes)
sorted_marxes


# In[147]:

marxes.sort()
marxes


# In[149]:

numbers = [2, 1, 4.0, 3]
numbers.sort()
numbers


# In[150]:

numbers.sort(reverse=True)
numbers


# In[151]:

marxes = ['Groucho', 'Chico', 'Harpo']
len(marxes)


# In[158]:

a=[1,2,3]
b=a.copy()
c=list(a)
d=a[:]


# In[165]:

empty_tuple = ()
empty_tuple


# In[169]:

one='Grucho',
one


# In[171]:

marx='apple','bag','bottle'
marx


# In[174]:

marx_tuple = ('Groucho', 'Chico', 'Harpo')
a,b,c=marx_tuple
a


# In[175]:

password = 'swordfish'
icecream = 'tuttifrutti'
password, icecream = icecream, password
password


# In[179]:

marx_list = ['Groucho', 'Chico', 'Harpo']
tuple(marx_list)


# In[180]:

empty_dict = {}
empty_dict


# In[182]:

bierce = {
"day": "A period of twenty-four hours, mostly misspent",
"positive": "Mistaken at the top of one's voice",
"misfortune": "The kind of fortune that never misses",
}


# In[183]:

bierce


# In[186]:

lol = [ ['a', 'b'], ['c', 'd'], ['e', 'f'] ]
dict(lol)


# In[187]:

lot = [ ('a', 'b'), ('c', 'd'), ('e', 'f') ]
dict(lot)


# In[188]:

tol = ( ['a', 'b'], ['c', 'd'], ['e', 'f'] )
dict(tol)


# In[190]:

los = [ 'ab', 'cd', 'ef' ]
dict(los)


# In[191]:

tos = ( 'ab', 'cd', 'ef' )
dict(tos)


# In[192]:

pythons = {
'Chapman': 'Graham',
'Cleese': 'John',
'Idle': 'Eric',
'Jones': 'Terry',
'Palin': 'Michael',
}


# In[194]:

pythons


# In[195]:

pythons['Gilliam'] = 'Gerry'
pythons


# In[197]:

others = { 'Marx': 'Groucho', 'Howard': 'Moe' }
pythons.update(others)
pythons


# In[199]:

first = {'a': 1, 'b': 2}
second = {'b': 'platypus'}
first.update(second)
first


# In[200]:

del pythons['Marx']
pythons


# In[201]:

pythons.clear()
pythons


# In[202]:

pythons = {'Chapman': 'Graham', 'Cleese': 'John','Jones': 'Terry', 'Palin': 'Michael'}
'Chapman' in pythons


# In[4]:

signals = {'green': 'go', 'yellow': 'go faster', 'red': 'smile for the camera'}
signals.keys()
signals.values()


# In[9]:

drinks = {
'martini': {'vodka', 'vermouth'},
'black russian': {'vodka', 'kahlua'},
'white russian': {'cream', 'kahlua', 'vodka'},
'manhattan': {'rye', 'vermouth', 'bitters'},
'screwdriver': {'orange juice', 'vodka'}
}


# In[10]:

for name, contents in drinks.items():
    if 'vodka' in contents:
        print(name)


# In[11]:

for name, contents in drinks.items():
    if 'vodka' in contents and not ('vermouth' in contents or 'cream' in contents):
        print(name)


# In[13]:

for name, contents in drinks.items():
    if contents & {'vermouth', 'orange juice'}:
        print(name)


# In[15]:

for name, contents in drinks.items():
    if 'vodka' in contents and not contents & {'vermouth', 'cream'}:
        print(name)


# In[ ]:



