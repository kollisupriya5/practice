

          ##CHAPTER-4

disaster = True

if disaster:

    print("Woe!")

else:

    print("Whee!")

#O/P-Woe!

#------------------------------------------

furry = True

small = True

if furry:

    if small:

        print("It's a cat.")

    else:

        print("It's a bear!")

else:

    if small:

        print("It's a skin!")

    else:

        print("It's a human. or a hairless bear.")

#o/p-It's a cat.

#-----------------------------------------------------
color = "puce"

if color == "red":

    print("It's a tomato")

elif color == "green":

    print("It's a green pepper")

elif color == "bee purple":

    print("I don't know what it is, but only bees can see it")

else:

    print("I've never heard of the color", color)

#o/p-I've never heard of the color puce

#-------------------------------------------------

some_list = []

if some_list:

    print("There's something in here")

else:

    print("Hey, it's empty!")

#o/p-Hey, it's empty!

#------------------------------------------------

count = 1

while count <= 5:

    print(count)

    count += 1

#o/p-1 2 3 4 5

#--------------------------------------------------

while True:

    stuff = input("String to capitalize [type q to quit]: ")

    if stuff == "q":

        break

    print(stuff.capitalize())
#o/p:
#1.String to capitalize [type q to quit]: a
A
#2.String to capitalize [type q to quit]: z
Z
#3.String to capitalize [type q to quit]: q

#-----------------------------------------------------

while True:

    value = input("Integer, please [q to quit]: ")

    if value == 'q': # quit

        break

    number = int(value)

    if number % 2 == 0: # an even number

        continue

    print(number, "squared is", number*number)

#1.Integer, please [q to quit]: 2
#2.Integer, please [q to quit]: 3
# 3 squared is 9
#3.Integer, please [q to quit]: 4
#4.Integer, please [q to quit]: 5
# 5 squared is 25
#5.Integer, please [q to quit]: q

#--------------------------------------------

numbers = [2, 3, 5]

position = 0

while position < len(numbers):

    number = numbers[position]

    if number % 2 == 0:

        print('Found even number', number)

        break

    position += 1

else: # break not called

        print('No even number found')

#o/p:Found even number 2

#-------------------------------------------------

rabbits = ['Flopsy', 'Mopsy', 'Cottontail', 'Peter']

current = 0

while current < len(rabbits):

    print(rabbits[current])

    current += 1
#o/p:
#Flopsy
#Mopsy
#Cottontail
#Peter

#---------------------------------------

word = 'cat'

for letter in word:

    print(letter)
#o/p:
#c
#a
#t

#----------------------------------------
cheeses = []

for cheese in cheeses:

    print('This shop has some lovely', cheese)

    break

else: # no break means no cheese

    print('This is not much of a cheese shop, is it?')

#o/p:This is not much of a cheese shop, is it?

#-----------------------------------------

cheeses = []

found_one = False

for cheese in cheeses:

    found_one = True

    print('This shop has some lovely', cheese)

    break

if not found_one:

    print('This is not much of a cheese shop, is it?')

#o/p:This is not much of a cheese shop, is it?

#--------------------------------------

a_list = []

for number in range(1,6):

    if number % 2 == 1:

        a_list.append(number)

#o/p:a_list

#[1, 3, 5]

#--------------------------------

rows = range(1,4)

cols = range(1,3)

for row in rows:

    for col in cols:

        print(row, col)
#o/p:
#1 1
#1 2
#2 1
#2 2
#3 1
#3 2

#---------------------------------------
rows = range(1,4)

cols = range(1,3)

cells = [(row, col) for row in rows for col in cols]

for cell in cells:

    print(cell)
#o/p:
#(1, 1)
#(1, 2)
#(2, 1)
#(2, 2)
#(3, 1)
#(3, 2)

#-------------------------------------

def commentary(color):

    if color == 'red':

        return "It's a tomato."

    elif color == "green":

        return "It's a green pepper."

    elif color == 'bee purple':

        return "I don't know what it is, but only bees can see it."

    else:

        return "I've never heard of the color " + color + "."

commentary('blue')

#"I've never heard of the color blue."

commentary('red')

#"It's a tomato."

#--------------------------------------

def outer(a, b):

    def inner(c, d):

        return c + d

    return inner(a, b)

outer(4,7)

#11

#---------------------------------------

def my_range(first=0, last=10, step=1):

    number = first

    while number < last:

        yield number

        number += step

#--------------------------------------------------
def document_it(func):

    def new_function(*args, **kwargs):

        print('Running function:', func.__name__)

        print('Positional arguments:', args)

        print('Keyword arguments:', kwargs)

        result = func(*args, **kwargs)

        print('Result:', result)

        return result

    return new_function

def add_ints(a, b):

    return a + b

add_ints(1,4)

#5

cooler_add_ints = document_it(add_ints)

cooler_add_ints(1, 4)

#Running function: add_ints
#Positional arguments: (1, 4)
#Keyword arguments: {}
#Result: 5

#5
#------------------------------------------
def square_it(func):

    def new_function(*args, **kwargs):

        result = func(*args, **kwargs)

        return result * result

    return new_function

add_ints(3, 5)

#8

#---------------------------------------------
short_list = [1, 2, 3]

position = 5

try:

    short_list[position]

except:

    print('Need a position between 0 and', len(short_list)-1, ' but got',position)

#Need a position between 0 and 2  but got 5

#---------------------------------------------

short_list = [1, 2, 3]

while True:

    value = input('Position [q to quit]? ')

    if value == 'q':

        break

    try:

        position = int(value)

        print(short_list[position])

    except IndexError as err:

        print('Bad index:', position)

    except Exception as other:

        print('Something else broke:', other)
#o/p:
#Position [q to quit]? 3
#Bad index: 3
#Position [q to quit]? 2
#3
#Position [q to quit]? 0
#1
#Position [q to quit]? 1
#2
#Position [q to quit]? q

​


