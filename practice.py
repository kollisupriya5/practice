
                    ##CHAPTER 1

# In[1]:

cliches = [
"At the end of the day",
"Having said that",
"The fact of the matter is",
"Be that as it may",
"The bottom line is",
"If you will",
]
print(cliches[3])


# In[8]:

print(cliches[0])


# In[18]:

quotes = {
"Moe": "A wise guy, huh?",
"Larry": "Ow!",
"Curly": "Nyuk nyuk!",
}
stooge = "Larry"
print(stooge,'says:', quotes[stooge])

                  ##CHAPTER 2
# In[37]:

a=2/3
print(a)


# In[38]:

type(a)


# In[39]:

b=a
print(b)


# In[42]:

a='abc'
print(a)
type(a)

       ##Integers

# In[43]:

5


# In[44]:

0


# In[45]:

05


# In[47]:

1+2


# In[52]:

-123


# In[54]:

4 + 3 - 2 - 1 + 6


# In[56]:

5+9   +   3


# In[59]:

6*7*2*3*1


# In[65]:

9/6


# In[67]:

9//6


# In[69]:

10/0


# In[84]:

a=95
a=a-2
a+=3
print(a)


# In[83]:

a = 95
temp = a - 3
a = temp
print(a)


# In[86]:

9%5


# In[88]:

divmod(9,5)

        ##Precedence

# In[89]:

2 + 3 * 4


# In[95]:

(2+3)*4

        ##Bases

# In[100]:

0b100  #base 2


# In[108]:

0o7  #base 8


# In[121]:

0xf   #base 16

       ##Type Conversions

# In[126]:

float(98.6)


# In[129]:

True+2


# In[137]:

False+2

       ##How Big Is an int?

# In[149]:

googol = 10**100 
googol
googol * googol
  

             ##STRINGS##
      ##Create with Quotes

# In[152]:

'A "two by four" is actually 1 1⁄2" × 3 1⁄2".'


# In[158]:

poem2 = '''I do not like thee, Doctor Fell.
   The reason why, I cannot tell.
       But this I know, and know full well:
          I do not like thee, Doctor Fell.
'''


# In[160]:

print(poem2)


# In[162]:

poem2


# In[168]:

bottles = 99
base = ''
base += 'current inventory: '
base += str(bottles)
base

     ##Escape with \

# In[177]:

palindrome = 'A man,\nA plan,\nA canal:\nPanama.'
print(palindrome)


# In[182]:

print('123456 \tabc')
print('1\t,2\t,4\t')


# In[186]:

testimony = "\"I did nothing!\" he said. \"Not that either! Or the other thing.\""

print(testimony)


# In[195]:

speech = 'Today we honor our friend,the backslash: \\.'
print(speech)

      ##Combine with +

# In[197]:

'Release the kraken! ' + 'At once!'


# In[198]:

"My word! " "A gentleman caller!"


# In[206]:

a = 'Duck!'
b = a
c = 'Grey Duck!'
a + b + c


     ##Duplicate with *
# In[210]:

start = 'Na ' * 4 + '\n'
middle = 'Hey ' * 3 + '\n'
end = 'Goodbye.'
print(start + start + middle + end)

      ##Extract a Character with []

# In[212]:

name = 'Henny'
name.replace('H', 'P')

# In[215]:

'P' + name[1:]


# In[216]:

letters = 'abcdefghijklmnopqrstuvwxyz'
letters[0]

letters[14]

     ##Slice with [ start : end : step ]

# In[218]:

letters = 'abcdefghijklmnopqrstuvwxyz'
letters[:]


# In[230]:

letters[-3]


# In[227]:

letters[12:15]


# In[238]:

letters[-6:-3]
letters[::10]


# In[241]:

letters[4:26:3]

       ##Get Length with len()

# In[246]:

len(letters)


# In[248]:

empty=""
len(empty)

       ##Split with split()
# In[256]

todos = 'get gloves,get mask,give cat vitamins,call ambulance'
todos.split(',')
       
# In[259]

todos.split()

      ##Combine with join()

# In[260]

crypto_list = ['Yeti', 'Bigfoot', 'Loch Ness Monster']
crypto_string = ', '.join(crypto_list)
print('Found and signing book deals:', crypto_string)

     ##Playing with Strings

# In[265]

poem = '''All that doth flow we cannot liquid name
Or else would fire and water be the same;
But that is liquid which is moist and wet
Fire that property can never get.
Then 'tis not cold that doth the fire put out
But 'tis the wet that makes it die, no doubt.'''
poem[:13]

# In[266]

len(poem)

poem.startswith('All')

poem.endswith('That\'s all, folks!')

word = 'the'
poem.find(word)

poem.rfind(word)

poem.count(word)

poem.isalnum()

      ##Case and Alignment
# In [275]

setup = 'a duck goes into a bar...'
setup.strip('.')

setup.capitalize()

setup.title()

setup.upper()

setup.lower()

setup.swapcase()

setup.center(30)

setup.ljust(30)

setup.rjust(30)

     ##Substitute with replace()

#In [317]

setup.replace('duck', 'marmoset')

setup.replace('a ', 'a famous ', 100)

setup.replace('a', 'a famous', 100) 


 




