import unittest
import distance as dst

class euclideantest1(unittest.TestCase):  
    def test_euclidean1(self):
        p=[1,2]
        q=[3,4]
        self.assertEqual(dst.euclidean1(p,q),2.8284271247461903)
    def test_euclidean2(self):
        p=[1,2]
        q=[3,4]
        self.assertNotEqual(dst.euclidean1(p,q),2.8284271247461903)
    def test_euclidean3(self):
        p=[1,2,3]
        q=[3,4]
        with self.assertRaises(IndexError):
            dst.euclidean1(p,q)
    def test_manhattan1(self):
        p=[2,3]
        q=[4,1]
        self.assertEqual(dst.manhattan1(p,q),4)
    def test_manhattan2(self):
        p=[2,3]
        q=[4,1]
        self.assertNotEqual(dst.manhattan1(p,q),4)
    def test_manhattan3(self):
        p=[2,3]
        q=[4,1]
        with self.assertRaises(IndexError):
            dst.manhattan1(p,q)
    def test_pearson1(self):
        p=(63,68,86,72,73,74)
        q=(87,100,110,111,70,79)
        self.assertEqual(dst.pearson1(p,q),0.32961238803118453)
    def test_pearson2(self):
        p=(63,68,86,72,73,74,75,76)
        q=(87,100,110,111,70,79,80,81)
        self.assertEqual(dst.pearson1(p,q), 2268390333418977)
    def test_pearson3(self):
        p=(63,68,86,72,73,74,75,76)
        q=(87,100,110,111,70,79,80,81)
        with self.assertRaises(IndexError):
            dst.pearson1(p,q)
    def test_chebyshev1(self):
        p=[1,2,3,4]
        q=[5,7,2,3]
        self.assertEqual(dst.chebyshev(p,q), 5)
    def test_chebyshev2(self):
        p=[1,2,3,4]
        q=[5,7,2,3]
        self.assertNotEqual(dst.chebyshev(p,q), 5)
    def test_chebyshev3(self):
        p=[1,2,3,4]
        q=[5,7,2,3,4]
        with self.assertRaises(IndexError):
            dst.chebyshev(p,q)
    
    
if __name__ == '__main__':
    unittest.main()
